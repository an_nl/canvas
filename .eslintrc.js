module.exports = {
    parser: 'babel-eslint',
    env: {
      browser: true,
      es6: true,
      jest: true
    },
    extends: [
      'airbnb', 'prettier'
    ],
    globals: {
      Atomics: 'readonly',
      SharedArrayBuffer: 'readonly',
    },
    parserOptions: {
      ecmaFeatures: {
        jsx: true,
      },
      ecmaVersion: 2018,
      sourceType: 'module',
    },
    plugins: [
      'react', 'prettier'
    ],
    settings: {
      "import/resolver": {
        "node": {
          "paths": ["src"],
          "extensions": [".js", ".jsx", ".ts", ".tsx"]
        }
      }
    },
    rules: {
      'quotes': ['error', 'double'],
      'linebreak-style': 0,
      'react/jsx-filename-extension': [1, { 'extensions': ['.js', '.jsx'] }],
      'comma-dangle': [2, "never"],
      'react/jsx-indent': ['error', 4],
      'import/no-extraneous-dependencies': 0,
      'no-unused-vars': 'error',
      'react/prop-types': 0,
      'react/destructuring-assignment': 0,
      'react/jsx-pascal-case': 0,
      'max-len': ["error", { "code": 90 }]
    },
  };
  