## Install

$ git clone https://an_nl@bitbucket.org/an_nl/canvas.git

$ cd PROJECT

$ npm install

## Available Scripts

In the project directory, you can run:

$ npm start

Runs the app in the development mode.
Open http://localhost:8082/ to view it in the browser.
The page will reload if you make edits.


$ npm test

Launches the test runner in the interactive watch mode.
See the section about running tests for more information.


$ npm run build

Builds the app for production to the build folder.