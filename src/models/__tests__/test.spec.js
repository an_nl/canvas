import СanvasEntity from "../СanvasEntity";

test("is the canvas class constructor created", () => {
    const canvas = new СanvasEntity();
    const mas = [];

    expect(canvas.canvasString).toBe("");
    expect(canvas.canvasMatrix).toStrictEqual(mas);
    expect(canvas.yBorder).toBe("|");
    expect(canvas.xBorder).toBe("");

    expect(canvas.currentCommand).toBe(null);
    expect(canvas.commands).toBe(null);
    expect(canvas.width).toBe(null);
    expect(canvas.height).toBe(null);
});