class СanvasEntity {
    constructor() {
        this.canvasString = "";
        this.canvasMatrix = [];

        this.yBorder = "|";
        this.xBorder = "";

        this.currentCommand = null;
        this.commands = null;
        this.width = null;
        this.height = null;
    }

    createCanvas() {
        this.canvasMatrix.length = this.height;

        // 2 - borders
        for(let i = 0; i < this.canvasMatrix.length; i += 1) {
            this.canvasMatrix[i] = [];
            this.canvasMatrix[i].length = this.width - 2;

            for(let j = 0; j < this.canvasMatrix[i].length; j += 1) {
                if(this.canvasMatrix[i][j] !== "x") {
                    this.canvasMatrix[i][j] = " ";
                }
            }
        }

        this.convertMasToString();
    }

    checkIsCanvasCreated() {
        const mas = this.canvasString.split("\n");

        // TO_DO
        // + 2 + 1 = borders plus empty string
        if(mas.length === this.height + 2 + 1) {
            this.initDraw();
        } else {
            throw "Error"
        }
    }

    initDraw() {
        this.commands.forEach(item => {
            const [rule, ...rest] = item.split(" ");

            if(rule === "L") {
                this.drawLine(item);
            }

            if(rule === "R") {
                this.drawRectangle(item);
            }

            if(rule === "B") {
                this.fillBucket(item);
            }
        });
    }

    getCanvasString() {
        return this.canvasString
    }

    setCanvas(commands) {
        this.commands = commands;
        const [rule, width, height] = this.commands[0].split(" ");
        
        this.width = +width;
        this.height = +height;

        // TO_DO
        if(rule === "C") {
            this.currentCommand = rule;
            this.createCanvas(this.commands[0]);
        } else {
            alert("Canvas cannot be created")
        }
    }
    
    fillBucket(B) {
        const [rule, xArg, yArg, color] = B.split(" ");
        this.currentCommand = rule;

        const fill = (yNew, xNew) => {
            const yCondition = yArg > 0 < this.height;
            const xCondition = xArg > 0 < this.width;
            const isNeedToReplace = this.canvasMatrix[yNew][xNew] === " ";

            if(
                yCondition && 
                xCondition && 
                isNeedToReplace
            ) {
                this.canvasMatrix[yNew][xNew] = color;
                // TO_DO
                if(yNew > 0) {
                    fill(yNew - 1, xNew);
                }

                if(yNew === 0 || xNew > 0) {
                    fill(yNew, xNew + 1);
                    fill(yNew, xNew - 1);
                }
            }
        }

        fill(+yArg, +xArg)

        this.convertMasToString();
    }

    // eslint-disable-next-line class-methods-use-this
    isAllowedToDrawLine(x1, y1, x2, y2) {
        const isHorisontalLine = y1 === y2;
        const isVerticalLine = x1 === x2;

        if(isHorisontalLine || isVerticalLine) {
            return true
        }

        return false
    }

    drawLine(L) {
        const [rule, x1, y1, x2, y2] = L.split(" ");
        this.currentCommand = rule;

        const isValid = this.isAllowedToDrawLine(x1, y1, x2, y2);

        const draw = () => {
            for(let i = 0; i < this.canvasMatrix.length; i += 1) {
                for(let j = 0; j < this.canvasMatrix[i].length; j += 1) {
    
                    const yCondition = j >= x1 - 1 && j <= x2 - 1;
                    const xCondition = i >= y1 - 1 && i <= y2 - 1;
    
                    if(xCondition && yCondition) {
                        this.canvasMatrix[i][j] = "x";
                    }
                }
            }

            if(this.currentCommand === "L") {
                this.convertMasToString();
            }
        }

        if(isValid) {
            draw();
        } else {
            alert(`${rule} Only horizontal or vertical lines are allowed`);
        }
    }

    drawRectangle(R) {
        // eslint-disable-next-line prefer-const
        let [rule, x1, y1, x2, y2] = R.split(" ");

        // 2 - borders
        x1 -= 2;
        x2 -= 2;

        const r1 = `R ${x1} ${y1} ${x1} ${y2}`;
        this.drawLine(r1);

        const r2 = `R ${x2} ${y1} ${x2} ${y2}`;
        this.drawLine(r2);

        const r3 = `R ${x1} ${y1} ${x2} ${y1}`;
        this.drawLine(r3);

        const r4 = `R ${x1} ${y2} ${x2} ${y2}`;
        this.drawLine(r4);

        this.convertMasToString();
    }

    convertMasToString() {
        this.xBorder = "";
        
        for(let i = 0; i < this.width; i += 1) {
            this.xBorder += "-";
        }
    
        this.xBorder += "\n";
        this.canvasString += this.xBorder;


        this.canvasMatrix.forEach(yLine => {
            this.canvasString += `${this.yBorder}${yLine.join("")}${this.yBorder}\n`;
        });

        this.canvasString += this.xBorder;

        // console.log(this.strResult, this.canvas);
        
        if(this.currentCommand === "C") {
            try {
                this.currentCommand = "";
                this.checkIsCanvasCreated();
            } catch {
                alert("Canvas is not created!");
            }
        }

        if(this.currentCommand === "B") {
            this.currentCommand = "";
            alert("It's OK. You may download result.");
        }
    }
}

export default СanvasEntity