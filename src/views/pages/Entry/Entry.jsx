import React, { useState } from "react";
import styles from "./Entry.module.scss";
import Upload from "../../components/Upload";
import Download from "../../components/Download";

function Entry() {
    const [href, setHref] = useState("");
    const [currentResult, setCurrentResult] = useState("");

    return (
        <div className={styles.wrapperParent}>
            <div className={styles.wrapperContent}>
                <Upload
                  setCurrentResult={setCurrentResult}
                />
            </div>

            <div className={styles.wrapperContent}>
                <Download
                  currentResult={currentResult}
                  href={href}
                  setHref={setHref}
                />
            </div>
        </div>
    )
}

export default Entry;