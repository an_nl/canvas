import React from "react";
import styles from "./Download.module.scss";

function Download(props) {
    const {
        currentResult,
        href,
        setHref
    } = props;

    const shapeBlob = () => {
        const blob = new Blob([currentResult], {type: "text/plain"});
        setHref(URL.createObjectURL(blob))
    }

    return (
        <a
          className={styles.href}
          href={href}
          onClick={shapeBlob}
          download="output.txt"
        >
            Download
        </a>
    )
}

export default Download;