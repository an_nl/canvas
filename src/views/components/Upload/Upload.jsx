/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState } from "react";
import СanvasEntity from "../../../models/СanvasEntity";
import styles from "./Upload.module.scss";

function Upload(props) {
    const { setCurrentResult } = props;

    const [valUpload, setValUpload] = useState("");

    const onChange = (e) => {    
        const file = e.target.files[0];
        const fileReader = new FileReader();       

        // eslint-disable-next-line no-unused-vars
        fileReader.onload = event => {
            const canvas = new СanvasEntity();
            canvas.setCanvas(fileReader.result.split(/\n/));
            setCurrentResult(canvas.getCanvasString());
        };

        fileReader.readAsText(file);
        setValUpload("");
    }

    return (
        <>
            <input
              id="upload-button"
              type="file"
              className={styles.input}
              onChange={onChange}
              value={valUpload}
            />

            <label
              className={styles.label}
              htmlFor="upload-button"
            >
                <span>Upload</span>
            </label>
        </>
    )
}

export default Upload;