import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import Entry from "./views/pages/Entry";
import "./assets/global.scss";

ReactDOM.render(
    <BrowserRouter>
        <Entry />
    </BrowserRouter>,
    document.getElementById("root")
);