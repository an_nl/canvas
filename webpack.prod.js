const merge = require("webpack-merge");
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const common = require("./webpack.common.js");

module.exports = merge(common, {
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].[hash].css",
            chunkFilename: "[id].[hash].css"
        })
    ],
    module: {
        rules: [{
            test: /\.module\.s(a|c)ss$/,
            loader: [
                MiniCssExtractPlugin.loader,
                {
                    loader: "css-loader",
                    options: {
                        modules: true
                    }
                },
                {
                    loader: "sass-loader"
                }
            ]
        }, {
            test: /\.s(a|c)ss$/,
            exclude: /\.module.(s(a|c)ss)$/,
            loader: [
                MiniCssExtractPlugin.loader,
                "css-loader",
                {
                    loader: "sass-loader"
                }
            ]
        }, {
            test: /\.(js|jsx)$/,
            exclude: [/node_modules/],
            use: ["babel-loader"]
        }]
    },
    mode: "production"
});