const merge = require("webpack-merge");
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const common = require("./webpack.common.js");

module.exports = merge(common, {
    mode: "development",
    devtool: "cheap-module-eval-source-map",
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        })
    ],
    module: {
        rules: [{
            test: /\.module\.s(a|c)ss$/,
            loader: [
                "style-loader",
                {
                    loader: "css-loader",
                    options: {
                        modules: true
                    }
                },
                {
                    loader: "sass-loader"
                }
            ]
        }, {
            test: /\.s(a|c)ss$/,
            exclude: /\.module.(s(a|c)ss)$/,
            loader: [
                "style-loader",
                "css-loader",
                {
                    loader: "sass-loader"
                }
            ]
        }, {
            test: /\.(js|jsx)$/,
            exclude: [/node_modules/],
            use: ["babel-loader"]
        }]
    },
    devServer: {
        historyApiFallback: true,
        port: 8082,
        lazy: false,
        disableHostCheck: true
    }
});