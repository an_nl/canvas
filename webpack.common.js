const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const HtmlWebPackPlugin = require("html-webpack-plugin");
// const Dotenv = require("webpack-dotenv-plugin");
const path = require("path");

module.exports = {
    entry: "./src/index.jsx",
    output: {
        path: path.resolve(__dirname, "./dist"),
        filename: "main.js",
        publicPath: "dist/"
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: "./index.html",
            filename: "index.html",
            favicon: "./src/assets/favicon.ico"
        }),
        // new Dotenv({
        //     path: './.env.sample'
        // })
    ],
    resolve: {
        extensions: [".jsx", ".js", ".json", '.scss']
    },
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                extractComments: true
            })
        ]
    },
    module: {
        rules: [{
            test: /\.(js|jsx)$/,
            exclude: [/node_modules/],
            use: ["babel-loader"]
        }]
    }
};